# README #

Kasutatud allikad:
https://enos.itcollege.ee/~jpoial/algoritmid/searchsort.html - kahendotsimine ja pistemeetod
https://stackoverflow.com/questions/42326456/java-binary-insertion-sort-troubleshoot - abi System.arraycopy meetodi kasutamisel

## Command line examples. Näidete kasutamine käsurealt ##
#### Compilation. Kompileerimine: ####

```
#!bash

javac -cp src src/IntSorting.java
```

#### Execution. Käivitamine: ####

```
#!bash

java -cp src IntSorting
```


### Usage of tests. Testide kasutamine ###
#### Compilation of a test. Testi kompileerimine: ####

```
#!bash

javac -encoding utf8 -cp 'src:test:test/junit-4.13.1.jar:test/hamcrest-core-1.3.jar' test/IntSortingTest.java

```
In Windows replace colons by semicolons. Sama Windows aknas (koolonite asemel peavad olema semikoolonid):

```
#!bash

javac -encoding utf8 -cp 'src;test;test/junit-4.13.1.jar;test/hamcrest-core-1.3.jar' test/IntSortingTest.java


```

#### Running a test. Testi käivitamine: ####

```
#!bash

java -cp 'src:test:test/junit-4.13.1.jar:test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore IntSortingTest
```

The same for Windows. Sama Windows aknas (koolonite asemel semikoolonid):

```
#!bash

java -cp 'src;test;test/junit-4.13.1.jar;test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore IntSortingTest
```
